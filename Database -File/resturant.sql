-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 13, 2019 at 10:59 AM
-- Server version: 10.3.15-MariaDB
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `resturant`
--

-- --------------------------------------------------------

--
-- Table structure for table `catrgiries`
--

CREATE TABLE `catrgiries` (
  `catg_id` int(11) NOT NULL,
  `Name` varchar(255) NOT NULL,
  `Descrbtion` varchar(255) NOT NULL,
  `ordering` int(11) NOT NULL,
  `Date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `catrgiries`
--

INSERT INTO `catrgiries` (`catg_id`, `Name`, `Descrbtion`, `ordering`, `Date`) VALUES
(1, 'Menu', 'Best Desert Food', 1, '2019-07-28'),
(2, 'Dessert', 'To Best Drink', 2, '2019-07-28'),
(3, 'Drink', 'the Best of Drink', 3, '2019-07-30');

-- --------------------------------------------------------

--
-- Table structure for table `items`
--

CREATE TABLE `items` (
  `item_id` int(11) NOT NULL,
  `Name` varchar(255) NOT NULL,
  `Descrbtion` varchar(255) NOT NULL,
  `price` varchar(255) NOT NULL,
  `Date` date NOT NULL,
  `cat_id` int(11) NOT NULL,
  `member_id` int(11) NOT NULL,
  `image` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `items`
--

INSERT INTO `items` (`item_id`, `Name`, `Descrbtion`, `price`, `Date`, `cat_id`, `member_id`, `image`) VALUES
(67, 'lemon with mint', 'lemon,mint', '4$', '2019-08-01', 3, 1, '634664_494201706010315261526.jpg'),
(68, 'milkshake', 'chocolate,milk,ice', '10$', '2019-08-01', 3, 1, '871954_drink-9.jpg'),
(69, 'french coffee', 'coffee', '7$', '2019-08-01', 3, 1, '344948_القهوة-1.jpg'),
(71, 'mix juice', 'vodka,creez', '8$', '2019-08-01', 3, 1, '555971_drink-10.jpg'),
(78, 'ban cake', 'honey,fruits', '15$', '2019-08-01', 2, 1, '761880_dessert-2.jpg'),
(79, 'fruits cake', 'fruits,cream', '25$', '2019-08-01', 2, 1, '61253_dessert-4.jpg'),
(81, 'cheese cake', 'perry,cream', '20$', '2019-08-01', 2, 1, '285154_dessert-5.jpg'),
(83, 'small ban cake', 'honey,fruits', '20$', '2019-08-01', 2, 1, '338453_dessert-7.jpg'),
(84, 'brownies', 'ice,chocolate', '20$', '2019-08-01', 2, 1, '88144_dessert-9.jpg'),
(89, 'eclair', 'dark,white chocolate', '24$', '2019-08-01', 2, 1, '315207_dessert-8.jpg'),
(93, 'shrimp salade', 'vegetables,shrimp', '17$', '2019-08-01', 1, 1, '908906_dish-5.jpg'),
(94, 'red chicken', 'chicken', '45$', '2019-08-01', 1, 1, '418007_dish-7.jpg'),
(98, 'healthy meal', 'honey,cheese,bread', '35$', '2019-08-01', 1, 1, '117077_dish-12.jpg'),
(99, 'grilled chicken', 'chicken,salade', '17$', '2019-08-01', 1, 1, '190120_dish-10.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `mes_id` int(11) NOT NULL,
  `Name` varchar(255) NOT NULL,
  `Email` varchar(255) NOT NULL,
  `Message` varchar(255) NOT NULL,
  `Date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`mes_id`, `Name`, `Email`, `Message`, `Date`) VALUES
(11, 'naguib', 'naguib@gamil.com', 'you are very good crew  continue', '2019-07-29'),
(12, '3ayed', 'ayed@gmail.com', '3aaaaa4 ya gd3an', '2019-08-05'),
(13, '3ayed', 'ayed@gmail.com', '3aaaaa4 ya gd3an', '2019-08-05');

-- --------------------------------------------------------

--
-- Table structure for table `resrvtion`
--

CREATE TABLE `resrvtion` (
  `res_id` int(11) NOT NULL,
  `Name` varchar(255) NOT NULL,
  `Email` varchar(255) NOT NULL,
  `Phone` int(11) NOT NULL,
  `Time` time NOT NULL,
  `Number_Guests` int(11) NOT NULL,
  `Date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `resrvtion`
--

INSERT INTO `resrvtion` (`res_id`, `Name`, `Email`, `Phone`, `Time`, `Number_Guests`, `Date`) VALUES
(5, 'naguib', 'naguib@gamil.com', 1147960092, '08:00:00', 2, '2019-07-30'),
(6, 'mostafa', 'mostafa@yahoo.com', 1111979058, '08:00:00', 4, '2019-06-17');

-- --------------------------------------------------------

--
-- Table structure for table `special`
--

CREATE TABLE `special` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `offer` varchar(255) NOT NULL,
  `img` varchar(255) NOT NULL,
  `date` date NOT NULL,
  `mem_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `special`
--

INSERT INTO `special` (`id`, `name`, `offer`, `img`, `date`, `mem_id`) VALUES
(4, 'breakfast', '20$', '183095_blog-6.jpg', '2019-08-01', 1),
(5, 'large sushi meal', '30$', '924936_69649.jpg', '2019-08-01', 1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `UserID` int(11) NOT NULL,
  `Username` varchar(255) NOT NULL,
  `Password` varchar(255) NOT NULL,
  `FullName` varchar(255) NOT NULL,
  `Email` varchar(255) NOT NULL,
  `GroupID` int(11) NOT NULL,
  `Date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`UserID`, `Username`, `Password`, `FullName`, `Email`, `GroupID`, `Date`) VALUES
(1, 'hassan', '123456', 'Hassan Elhawary', 'hassan@gmail.com', 1, '2019-07-30');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `catrgiries`
--
ALTER TABLE `catrgiries`
  ADD PRIMARY KEY (`catg_id`);

--
-- Indexes for table `items`
--
ALTER TABLE `items`
  ADD PRIMARY KEY (`item_id`),
  ADD KEY `cat` (`cat_id`),
  ADD KEY `mem` (`member_id`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`mes_id`);

--
-- Indexes for table `resrvtion`
--
ALTER TABLE `resrvtion`
  ADD PRIMARY KEY (`res_id`);

--
-- Indexes for table `special`
--
ALTER TABLE `special`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mem_1` (`mem_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`UserID`),
  ADD UNIQUE KEY `UserID` (`UserID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `catrgiries`
--
ALTER TABLE `catrgiries`
  MODIFY `catg_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `items`
--
ALTER TABLE `items`
  MODIFY `item_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=103;

--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `mes_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `resrvtion`
--
ALTER TABLE `resrvtion`
  MODIFY `res_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `special`
--
ALTER TABLE `special`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `UserID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `items`
--
ALTER TABLE `items`
  ADD CONSTRAINT `cat` FOREIGN KEY (`cat_id`) REFERENCES `catrgiries` (`catg_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `mem` FOREIGN KEY (`member_id`) REFERENCES `users` (`UserID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `special`
--
ALTER TABLE `special`
  ADD CONSTRAINT `mem_1` FOREIGN KEY (`mem_id`) REFERENCES `users` (`UserID`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
